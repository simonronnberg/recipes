import React, { Component } from 'react';
import './App.css';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddIcon from '@material-ui/icons/Add';
//import MenuAppBar from 'MenuAppBar';
import { withStyles } from 'material-ui/styles';
//import Icon from 'material-ui/Icon';
//import AddIcon from '@material-ui/icons/Add';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import autoBind from 'react-autobind';
import AddRecipeModal from './AddRecipeModal';
//import FloatingActionButtons from './FloatingActionButtons';

type State = {
  addRecipeModalOpen: boolean,
  //view: View,
};

const style = {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
};

export default class App extends Component<{}, State> {

  constructor() {
    super();

    //let startingView = 'home';
    /*{
      const hashParts = window.location.hash.split('#/');
      if (hashParts.length > 1) {
        startingView = hashParts[1];
      }
    }*/

    this.state = {
      addRecipeModalOpen: false,
      //view: startingView,
    };

    autoBind(this);
  }

  handleAddClick = (event, checked) => {
    this.setState({ addRecipeModalOpen: true });

  };

  handleRequestAbsenceComplete() {
    this.setState({ addRecipeModalOpen: false });
    this.updateRequests();
  }

  renderAddRecipeModal() {
    if (!this.state.addRecipeModalOpen) {
      return null;
    }
    return (
      <AddRecipeModal
        onClose={this.handleAddRecipeComplete}
      />
    );
  }

  render() {
    return (
      <div>
      <div>
        {this.renderAddRecipeModal()}
        <AppBar>
          <Toolbar>
            <IconButton color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit">
              Tidy Recipes
            </Typography>
            <Button
              className="add-button"
              color="primary"
              variant="raised"
              onClick={() => this.setState({ addRecipeModalOpen: !this.state.addRecipeModalOpen })}
              >
              Add recipe
            </Button>
          </Toolbar>
        </AppBar>
        </div>
        <div>
        <Button
          variant="fab"
          color="primary"
          aria-label="add"
          style={style}
          onClick={() => this.setState({ addRecipeModalOpen: !this.state.addRecipeModalOpen })}
          >
          <AddIcon />
        </Button>
        </div>
      </div>
    );
  }
};
