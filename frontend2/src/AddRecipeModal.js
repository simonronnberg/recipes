import React from 'react';
import {
  Button,
  TextField,
  Typography,
} from 'material-ui';
import autoBind from 'react-autobind';

//import ApiClient from './api';

//import type { AddRecipeRequest } from './api';

type AddRecipeModalProps = {
  onClose: () => void,
}

/*type AddRecipeModalState = {
  params: AddRecipeRequest,
};*/

const initialParams: AddRecipeRequest = {
  title: '',
  comment: '',
};

export default class AddRecipeModal extends React.Component<AddRecipeModalProps, AddRecipeModalState> {
  constructor() {
    super();

    this.state = {
      params: initialParams,
    };

    autoBind(this);
  }

  handleChange(name: string) {
    return (e: any) => {
      this.setState({
        params: {
          ...this.state.params,
          [name]: e.target.value,
        },
      });
    };
  }

  handleSave() {
  /*  ApiClient.AddRecipe.createRecipe(this.state.params).then(() => {
      this.props.onClose();
    });*/
  }

  render() {
    return (
      <div className="fixedModal" tabIndex={-1}>
        <Typography variant="title" id="modal-title" style={{ marginBottom: 20 }}>
          Add recipe
        </Typography>

        <form autoComplete="off" onSubmit={this.handleSave}>
          <div>
            <TextField
              id="title"
              type="string"
              label="Title"
              value={this.state.params.title}
              onChange={this.handleChange('title')}
              margin="normal"
            />
          </div>
          <div>
            <TextField
              id="comment"
              label="Comment"
              placeholder="Placeholder"
              multiline
              fullWidth
              value={this.state.params.comment}
              onChange={this.handleChange('comment')}
            />
          </div>

          <div style={{ marginTop: 40 }}>
            <Button color="primary" variant="raised" onClick={this.handleSave}>
              Add
            </Button>

            <Button onClick={this.props.onClose}>
              Cancel
            </Button>
          </div>
        </form>
      </div>
    );
  }
}
