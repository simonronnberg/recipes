import Mapper from 'rest-mapper';
import { API_URL } from './config';

let API = new Mapper({
    host : API_URL,

    cruds : {
        recipe : { baseURL : '/recipe' },
        tag : { baseURL : '/tag' },
        ingredient : { baseURL : '/ingredient' },
        unit : { baseURL : '/unit' },
    }
});

export default API;