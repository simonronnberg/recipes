import { SET_MODE } from './types';

export { loadRecipes } from './recipes';

export const setMode = (mode) => dispatch => {
    dispatch({
        type : SET_MODE,
        mode
    })
};