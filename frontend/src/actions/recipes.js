import { GET_ALL_RECIPES, SET_CURRENT_RECIPE } from './types';
import api from '../backend';

const loadingRecipes = () => ({
    type : GET_ALL_RECIPES,
    state: 'loading'
});

const loadedRecipes = (recipes) => ({
    type : GET_ALL_RECIPES,
    state : 'success',
    recipes
});

const errorLoadingRecipes = (err) => ({
    type : GET_ALL_RECIPES,
    state : 'error',
    err
});

export const loadRecipes = () => dispatch => {
    dispatch(loadingRecipes());
    return api.recipe.all()
        .then(response => dispatch(loadedRecipes(response.data)))
        .catch((error) => dispatch(errorLoadingRecipes(error)));
};