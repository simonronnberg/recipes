import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import RecipeList from './components/recipe_list';
import Recipe from './components/recipe';

export default (
    <Route path='/' component={App}>
        <IndexRoute component={RecipeList} />
        <Route path='recipes' component={RecipeList} />
        <Route path='recipe/:id' component={Recipe} />
    </Route>
);