import { combineReducers } from 'redux';
import { SET_MODE } from '../actions/types';
import RecipesReducer from './recipes';

const mode = (state = 'view', action) => {
    switch(action.type) {
        case SET_MODE:
            return action.mode;
            break;
    }
    return state;
};

const rootReducer = combineReducers({
    recipes : RecipesReducer,
    mode
});

export default rootReducer;