import { GET_ALL_RECIPES } from '../actions/types';

const recipes = (state = { loading: false, recipes: [], error: null}, action) => {
    switch(action.type) {
        case GET_ALL_RECIPES:
            switch(action.state) {
                case 'loading':
                    return {...state, loading: true};
                    break;
                case 'success':
                    return {...state, loading: false, error: null, recipes: action.recipes};
                    break;
                case 'error':
                    return {...state, loading: false, error: action.err, recipes: []};
                    break;
            }
            break;
    }
    return state;
};



export default recipes;