import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import { Router, hashHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';

import reducers from './reducers/index';

import routes from './routes';

require('./assets/stylesheets/base.scss');
require('./assets/stylesheets/lemonade.scss');
require('./assets/stylesheets/recipes.scss');

const logger = createLogger();

const store = applyMiddleware(thunk,
                              logger,
                              routerMiddleware(hashHistory))
              (createStore)
              (reducers);

ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory} routes={routes} />
    </Provider>,
    document.querySelector('#app'));