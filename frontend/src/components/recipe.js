import { connect } from 'react-redux';
import { loadRecipes, setMode } from '../actions/index'
import React, {Component} from "react";
import RecipeView from './recipe_view';
import RecipeEdit from './recipe_edit';

class Recipe extends Component {

    componentDidMount() {
        var {loading, load, loadRecipes, setMode, mode, id} = this.props;
        if (!loading && load) {
            loadRecipes();
        }
        if (id == 0 && mode == 'view') {
            setMode('edit');
        }
    }

    componentWillUnmount() {
        var { mode, setMode } = this.props;
        if (mode == 'edit') {
            // TODO: confirm edit cancel
            setMode('view');
        }
    };

    render() {
        var { mode, recipe } = this.props;
        if (mode == 'view' && recipe) {
            return <RecipeView recipe={recipe}/>
        } else if (mode == 'edit') {
            return <RecipeEdit recipe={recipe}/>
        } else {
            return <div className="hidden"></div>
        }
    }
}

const mapState = (state, ownProps) => {
    let results = {
        recipes: state.recipes,
        id: ownProps.params.id,
        recipe : state.recipes.recipes.find(r => r.id == ownProps.params.id),
        mode : state.mode,
        loading : state.recipes.recipes.loading,
        load : false
    };
    if (results.id != 0 && !results.recipe) {
        results.load = true;
    }
    return results;
};

export default connect(mapState, { loadRecipes, setMode })(Recipe);