import React, { Component } from "react";
import { Link } from 'react-router';
import { setMode } from '../actions/index';
import { connect } from 'react-redux';

const Top = (props) => {

    const toggleMode = () => {
        var { mode, setMode } = props;
        if (mode == 'view') {
            setMode('edit');
        } else {
            setMode('view');
        }
    };

    const toggle = props.mode == 'view' ? 'Edit' : 'View';

    return <div className="frame">
        <h3 className="bit-75"><Link to="/recipes">Recipes</Link></h3>
        <div className="bit-25"><a onClick={toggleMode}>{toggle}</a></div>
    </div>;
};

const mapState = ({ mode }) => ({
    mode
});

export default connect(mapState, { setMode })(Top);;