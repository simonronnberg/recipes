import React, {Component} from "react";

const RecipeIngredient = ({ingredient}) =>
    <div className="frame recipe-ingredient">
        <div className="bit-25 recipe-ingredient-amount">{ingredient.amount}{ingredient.unit.name}</div>
        <div className="bit-75 recipe-ingredient-text">{ingredient.ingredient.name}</div>
    </div>;

const RecipeInstruction = ({instruction}) =>
    <div className="frame recipe-instruction">
        <div className="bit-3em recipe-instruction-order">{instruction.order}.</div>
        <div className="bit-3em-rest recipe-instruction-text">{instruction.text}</div>
    </div>;


const RecipeItem = ({item, ingredients}) => {
    const itemIngredients = ingredients.filter(i => (item.id == 0 && i.item == null) ||
                                                    (item.id != 0 && i.item != null && i.item.id == item.id));
    if (itemIngredients.length == 0) {
        return <div className="hidden"></div>;
    }
    return <div className="frame recipe-item">
            <div className="bit-1 recipe-item-title">{item.name}</div>
            <div className="bit-1 recipe-item-ingredients">
                { itemIngredients.map(i => <RecipeIngredient ingredient={i} key={i.id}/>) }
        </div>
    </div>;
};

const RecipeView = ({recipe}) =>
    <div className="frame">
        <div className="bit-75">
            <div className="frame">
                <div className="bit-1 recipe-title">{recipe.name}</div>
                <div className="bit-1">
                    <div className="box-half-height box-shadow rounded">
                        <div className="box-content">
                            <img className="box-fill-back" src={recipe.bannerImageUrl} />
                        </div>
                    </div>
                </div>
                <div className="bit-1 recipe-text">{recipe.description}</div>
                <div className="bit-1 recipe-instructions">
                    <div className="frame">
                        <div className="bit-1 recipe-instructions-list">
                            { recipe.instructions.map(i => <RecipeInstruction instruction={i} key={i.id} />) }
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="bit-25 recipe-items">
            { recipe.items.map(item => <RecipeItem item={item}
                                                   ingredients={recipe.ingredients}
                                                   key={item.id}/>) }
            <RecipeItem item={{ id : 0, name: '' }} ingredients={recipe.ingredients} key="0"/>
        </div>
    </div>;

export default RecipeView;