import React, { Component } from 'react';
import Top from './top';
import Bottom from './bottom';

const App = (props) => (
    <div>
        <Top />
        <div className="frame">
            <div className="bit-1">
                {props.children}
            </div>
        </div>
        <Bottom />
    </div>
);

export default App;
