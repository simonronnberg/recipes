import React, {Component} from "react";
import { connect } from 'react-redux';
import { loadRecipes } from '../actions/index'
import { Link } from 'react-router';

const RecipeListEntry = ({ recipe }) =>
    <div className="bit-3">
        <div className="box box-shadow rounded">
            <Link to={{ pathname: `/recipe/${recipe.id}` }}>
                <div className="box-content">
                    <img className="box-fill-back" src={recipe.posterImageUrl} />
                    <div className="box-lower-right">
                        {recipe.name}
                    </div>
                </div>
            </Link>
        </div>
    </div>;

class RecipeList extends Component {

    componentDidMount() {
        var { loadRecipes } = this.props;
        loadRecipes();
    }

    render() {
        var { recipes } = this.props;

        var add = {
            id: 0,
            name : '',
            posterImageUrl : 'plus.png'
        };

        return (
            <div className="frame">
                { recipes.map(recipe => <RecipeListEntry recipe={recipe} key={recipe.id}/>) }
                <RecipeListEntry recipe={add}/>
            </div>
        );
    }
}

const mapState = (state) => ({
    recipes : state.recipes.recipes
});

export default connect(mapState, { loadRecipes })(RecipeList);