package se.darknova.app;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import org.slf4j.bridge.SLF4JBridgeHandler;
import se.darknova.app.rest.IngredientResource;
import se.darknova.app.rest.RecipeResource;
import se.darknova.app.rest.TagResource;
import se.darknova.app.rest.UnitResource;
import se.darknova.config.ConfigModule;
import se.darknova.db.DBModule;
import se.darknova.guava.GuavaRun;
import se.darknova.guava.GuavaServiceModule;
import se.darknova.http.HttpModule;

/**
 * @author simon
 */
public class Main {

    private static class AppModule extends AbstractModule {

        @SuppressWarnings("PointlessBinding") // rest resources need explicit bind
        protected void configure() {
            bind(RecipeResource.class);
            bind(IngredientResource.class);
            bind(TagResource.class);
            bind(UnitResource.class);
        }
    }

    public static void main(String[] args) {
        System.setProperty("applicationId", "recipes-backend");
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        Guice.createInjector(new ConfigModule(),
                             new DBModule(),
                             new GuavaServiceModule(),
                             new HttpModule(),
                             new AppModule())
            .getInstance(GuavaRun.class)
            .start();
    }
}
