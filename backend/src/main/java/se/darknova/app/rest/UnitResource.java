package se.darknova.app.rest;

import lombok.RequiredArgsConstructor;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.app.logic.UnitService;
import se.darknova.db.entity.Unit;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@Path("/api/unit")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class UnitResource {

    private final UnitService unitService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Unit> list() {
        return unitService.list();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Unit get(@PathParam("id") long id) {
        Optional<Unit> unit = unitService.get(id);
        if (!unit.isPresent()) {
            throw new NotFoundException();
        }
        return unit.get();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Unit create(Unit unit) {
        unitService.create(unit);
        return unit;
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Unit unit) {
        unitService.update(id, unit);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        unitService.delete(id);
        return Response.ok().build();
    }
}
