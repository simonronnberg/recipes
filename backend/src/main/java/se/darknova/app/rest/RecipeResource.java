package se.darknova.app.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import se.darknova.app.logic.RecipeService;
import se.darknova.db.entity.Ingredient;
import se.darknova.db.entity.Recipe;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@Path("/api/recipe/")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
public class RecipeResource {

    private final RecipeService recipeService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Recipe get(@PathParam("id") long id) {
        Optional<Recipe> recipe = recipeService.get(id);
        if (!recipe.isPresent())
            throw new NotFoundException();
        return recipe.get();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Recipe> get() {
        return recipeService.list();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Recipe create(Recipe recipe) {
        recipeService.create(recipe);
        return recipe;
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Recipe recipe) {
        recipeService.update(id, recipe);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        recipeService.delete(id);
        return Response.ok().build();
    }
}
