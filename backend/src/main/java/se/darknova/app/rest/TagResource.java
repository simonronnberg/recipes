package se.darknova.app.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.app.logic.TagService;
import se.darknova.db.entity.Tag;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@Path("/api/tag")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
public class TagResource {

    private final TagService tagService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tag> get() {
        return tagService.list();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Tag get(@PathParam("id") long id) {
        Optional<Tag> tag = tagService.get(id);
        if (!tag.isPresent()) {
            throw new NotFoundException();
        }
        return tag.get();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Tag create(Tag tag) {
        tagService.create(tag);
        return tag;
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Tag tag) {
        tagService.update(id, tag);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        tagService.delete(id);
        return Response.ok().build();
    }
}
