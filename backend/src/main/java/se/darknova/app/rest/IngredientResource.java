package se.darknova.app.rest;

import lombok.RequiredArgsConstructor;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.app.logic.IngredientService;
import se.darknova.db.entity.Ingredient;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Path("/api/ingredient/")
public class IngredientResource {

    private final IngredientService ingredientService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ingredient> list() {
        return ingredientService.list();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Ingredient get(@PathParam("id") long id) {
        Optional<Ingredient> ingredient = ingredientService.get(id);
        if (!ingredient.isPresent()) {
            throw new NotFoundException();
        }
        return ingredient.get();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Ingredient create(Ingredient ingredient) {
        ingredientService.create(ingredient);
        return ingredient;
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Ingredient ingredient) {
        ingredientService.update(id, ingredient);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        ingredientService.delete(id);
        return Response.ok().build();
    }
}
