package se.darknova.app.logic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.db.entity.Tag;
import se.darknova.db.mapper.TagMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@Singleton
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
public class TagService {

    private final TagMapper tagMapper;

    @Transactional
    public List<Tag> list() {
        return tagMapper.list();
    }

    @Transactional
    public Optional<Tag> get(long id) {
        return Optional.ofNullable(tagMapper.get(id));
    }

    @Transactional
    public Tag create(Tag tag) {
        tagMapper.insert(tag);
        return tag;
    }

    @Transactional
    public void update(long id, Tag tag) {
        tag.setId(id);
        tagMapper.update(tag);
    }

    @Transactional
    public void delete(long id) {
        tagMapper.delete(id);
    }
}
