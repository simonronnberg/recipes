package se.darknova.app.logic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.db.entity.Unit;
import se.darknova.db.mapper.UnitMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@Singleton
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
public class UnitService {

    private final UnitMapper unitMapper;

    @Transactional
    public List<Unit> list() {
        return unitMapper.list();
    }

    @Transactional
    public Optional<Unit> get(long id) {
        return Optional.ofNullable(unitMapper.get(id));
    }

    @Transactional
    public Unit create(Unit unit) {
        unitMapper.insert(unit);
        return unit;
    }

    @Transactional
    public void update(long id, Unit unit) {
        unit.setId(id);
        unitMapper.update(unit);
    }

    @Transactional
    public void delete(long id) {
        unitMapper.delete(id);
    }
}
