package se.darknova.app.logic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.db.entity.Ingredient;
import se.darknova.db.mapper.IngredientMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

/**
 * @author simon
 */
@Singleton
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
public class IngredientService {

    private final IngredientMapper ingredientMapper;

    @Transactional
    public List<Ingredient> list() {
        return ingredientMapper.list();
    }

    @Transactional
    public Optional<Ingredient> get(long id) {
        return Optional.ofNullable(ingredientMapper.get(id));
    }

    @Transactional
    public Ingredient create(Ingredient ingredient) {
        ingredientMapper.insert(ingredient);
        return ingredient;
    }

    @Transactional
    public void update(long id, Ingredient ingredient) {
        ingredient.setId(id);
        ingredientMapper.update(ingredient);
    }

    @Transactional
    public void delete(long id) {
        ingredientMapper.delete(id);
    }
}
