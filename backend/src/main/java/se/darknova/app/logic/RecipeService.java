package se.darknova.app.logic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.guice.transactional.Transactional;
import se.darknova.db.entity.Recipe;
import se.darknova.db.entity.RecipeIngredient;
import se.darknova.db.entity.RecipeInstruction;
import se.darknova.db.entity.RecipeItem;
import se.darknova.db.entity.Tag;
import se.darknova.db.mapper.RecipeIngredientMapper;
import se.darknova.db.mapper.RecipeInstructionMapper;
import se.darknova.db.mapper.RecipeItemMapper;
import se.darknova.db.mapper.RecipeMapper;
import se.darknova.db.mapper.TagMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author simon
 */
@Singleton
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
public class RecipeService {

    private final RecipeMapper recipeMapper;
    private final RecipeInstructionMapper recipeInstructionMapper;
    private final RecipeIngredientMapper recipeIngredientMapper;
    private final RecipeItemMapper recipeItemMapper;
    private final TagMapper tagMapper;

    @Transactional
    public Optional<Recipe> get(long id) {
        return Optional.ofNullable(recipeMapper.get(id));
    }

    @Transactional
    public List<Recipe> list() {
        return recipeMapper.list();
    }

    @Transactional
    public Recipe update(long id, Recipe recipe) {
        Optional<Recipe> current =
            id == 0 ? Optional.empty() : Optional.ofNullable(recipeMapper.get(id));
        if(current.isPresent()) {
            recipe.setId(id);
            recipeMapper.update(recipe);

            // delete instructions
            Set<Long> instructionIds = recipe.getInstructions()
                                             .stream()
                                             .filter(instruction -> instruction.getId() != 0)
                                             .map(RecipeInstruction::getId)
                                             .collect(Collectors.toSet());
            current.get()
                   .getInstructions()
                   .stream()
                   .filter(instruction -> !instructionIds.contains(instruction.getId()))
                   .forEach(instruction -> recipeInstructionMapper.remove(instruction.getId()));

            // delete ingredients
            Set<Long> ingredientIds = recipe.getIngredients()
                                            .stream()
                                            .filter(ingredient -> ingredient.getId() != 0)
                                            .map(RecipeIngredient::getId)
                                            .collect(Collectors.toSet());
            current.get()
                   .getIngredients()
                   .stream()
                   .filter(ingredient -> !ingredientIds.contains(ingredient.getId()))
                   .forEach(ingredient -> recipeIngredientMapper.remove(ingredient.getId()));

            // delete items
            Set<Long> itemIds = recipe.getItems()
                                      .stream()
                                      .filter(item -> item.getId() != 0)
                                      .map(RecipeItem::getId)
                                      .collect(Collectors.toSet());
            current.get()
                   .getItems()
                   .stream()
                   .filter(item -> !itemIds.contains(item.getId()))
                   .forEach(item -> recipeItemMapper.remove(item.getId()));

            // delete tags
            Set<Long> tagIds = recipe.getTags()
                                     .stream()
                                     .filter(tag -> tag.getId() != 0)
                                     .map(Tag::getId)
                                     .collect(Collectors.toSet());
            current.get()
                   .getTags()
                   .stream()
                   .filter(tag -> !tagIds.contains(tag.getId()))
                   .forEach(tag -> recipeMapper.deleteTag(recipe.getId(), tag.getId()));

            // update or insert tags
            recipe.getTags().forEach(tag -> {
                if(tag.getId() == 0) {
                    tag.setId(tagMapper.insert(tag));
                }
                recipeMapper.insertTag(recipe.getId(), tag.getId());
            });

            // update or insert items
            recipe.getItems().forEach(item -> {
                if(item.getId() == 0) {
                    recipeItemMapper.add(recipe.getId(), item);
                } else {
                    recipeItemMapper.update(item);
                }
            });

            // update or insert ingredients
            recipe.getIngredients().forEach(ingredient -> {
                if (ingredient.getId() == 0) {
                    recipeIngredientMapper.add(recipe.getId(), ingredient);
                } else {
                    recipeIngredientMapper.update(ingredient);
                }
            });

            // update order of instructions
            final int[] order = {1};
            // update or insert instructions
            recipe.getInstructions().forEach(instruction -> {
                instruction.setOrder(order[0]++);
                if (instruction.getId() == 0) {
                    recipeInstructionMapper.add(recipe.getId(), instruction);
                } else {
                    recipeInstructionMapper.update(instruction);
                }
            });
            return recipe;
        } else {
            return create(recipe);
        }
    }

    @Transactional
    public Recipe create(Recipe recipe) {
        final long id = recipeMapper.insert(recipe);
        recipe.setId(id);

        recipe.getTags().forEach(tag -> {
            if(tag.getId() <= 0) {
                tag.setId(tagMapper.insert(tag));
            }
            recipeMapper.insertTag(recipe.getId(), tag.getId());
        });
        recipe.getItems().forEach(item -> recipeItemMapper.add(id, item));
        recipe.getIngredients().forEach(ingredient -> recipeIngredientMapper.add(id, ingredient));
        recipe.getInstructions()
              .forEach(instruction -> recipeInstructionMapper.add(id, instruction));
        return recipe;
    }

    @Transactional
    public void delete(long id) {
        Optional<Recipe> current = Optional.ofNullable(recipeMapper.get(id));
        if(current.isPresent()) {
            current.get()
                   .getTags()
                   .forEach(tag -> recipeMapper.deleteTag(current.get().getId(), tag.getId()));
            current.get()
                   .getInstructions()
                   .forEach(instruction -> recipeInstructionMapper.remove(instruction.getId()));
            current.get()
                   .getIngredients()
                   .forEach(ingredient -> recipeIngredientMapper.remove(ingredient.getId()));
            current.get().getItems().forEach(item -> recipeItemMapper.remove(item.getId()));
            recipeMapper.delete(current.get().getId());
        }
    }
}
