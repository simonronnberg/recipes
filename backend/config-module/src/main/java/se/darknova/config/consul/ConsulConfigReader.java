package se.darknova.config.consul;

import com.google.common.net.HostAndPort;

import com.netflix.archaius.config.polling.PollingResponse;
import com.orbitz.consul.Consul;
import com.orbitz.consul.KeyValueClient;
import com.orbitz.consul.model.kv.Value;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * @author simon
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
@Slf4j
class ConsulConfigReader implements Callable<PollingResponse> {

    private final Provider<ConsulClientConfig> configProvider;

    @Override
    public PollingResponse call() throws Exception {
        ConsulClientConfig config = configProvider.get();
        if(!config.isEnabled()) {
            return PollingResponse.noop();
        }
        log.debug("Refreshing configuration from {}:{}/{}",
                  config.getHost(),
                  config.getPort(),
                  config.getPrefix());
        KeyValueClient keyValueClient = Consul.builder()
                                              .withHostAndPort(HostAndPort.fromParts(config.getHost(),
                                                                                     config.getPort()))
                                              .build()
                                              .keyValueClient();
        if(keyValueClient == null) {
            return PollingResponse.noop();
        }
        final Map<String, String> results = new HashMap<>();
        for(Value value : keyValueClient.getValues(config.getPrefix())) {
            results.put(value.getKey().replace("/", "."), value.getValueAsString().or(""));
        }
        return PollingResponse.forSnapshot(results);
    }
}

