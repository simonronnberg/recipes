package se.darknova.config.consul;

import com.google.inject.AbstractModule;
import com.netflix.archaius.api.Config;
import com.netflix.archaius.api.inject.RemoteLayer;

/**
 * @author simon
 */
public class ConsulConfigModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ConsulClientConfig.class).toInstance(new ConsulClientConfig());
        bind(Config.class).annotatedWith(RemoteLayer.class)
                          .toProvider(ConsulConfigProvider.class);
    }
}
