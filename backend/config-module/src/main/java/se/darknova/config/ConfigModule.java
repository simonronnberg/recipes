package se.darknova.config;

import com.google.inject.AbstractModule;
import com.netflix.archaius.cascade.InterpolatingCascadeStrategy;
import com.netflix.archaius.guice.ArchaiusModule;

import java.util.Arrays;
import java.util.List;

/**
 * @author simon
 */
public class ConfigModule extends AbstractModule {

    private static class MyCascadeStrategy extends InterpolatingCascadeStrategy {

        private final List<String> permutations = Arrays.asList("%s",
                                                                "%s-${environment}",
                                                                "%s-${applicationId}",
                                                                "%s-${applicationId}-${environment}");

        @Override
        protected List<String> getPermutations() {
            return permutations;
        }
    }

    @Override
    protected void configure() {
        install(new ArchaiusModule() {

            @Override
            protected void configureArchaius() {
                bindCascadeStrategy().toInstance(new MyCascadeStrategy());
            }
        });
    }
}
