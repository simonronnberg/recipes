package se.darknova.config.consul;

import com.netflix.archaius.api.Config;
import com.netflix.archaius.config.PollingDynamicConfig;
import com.netflix.archaius.config.polling.FixedPollingStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Provider;
import java.util.concurrent.TimeUnit;

/**
 * @author simon
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Inject))
class ConsulConfigProvider implements Provider<Config> {

    private final Provider<ConsulClientConfig> config;
    private volatile PollingDynamicConfig dynamicConfig;

    @Override
    public Config get() {
        log.info("Consul refresh rate is {}", config.get().getRefreshRate());
        return dynamicConfig = new PollingDynamicConfig(new ConsulConfigReader(config),
                                                        new FixedPollingStrategy(config.get().getRefreshRate(),
                                                                                 TimeUnit.SECONDS));
    }

    @PreDestroy
    public void shutdown() {
        if (dynamicConfig != null) {
            dynamicConfig.shutdown();
        }
    }
}
