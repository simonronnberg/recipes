package se.darknova.config.consul;

import com.netflix.archaius.api.annotations.Configuration;
import lombok.Data;

/**
 * @author simon
 */
@Configuration(prefix = "consul.http")
@Data
public class ConsulClientConfig {
    private String host = "localhost";
    private int port = 8500;
    private String prefix = "";
    private boolean enabled = false;
    private int refreshRate = 7;
}
