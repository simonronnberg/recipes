package se.darknova.http;

import se.darknova.guava.GuavaServiceRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.inject.Provider;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author simon
 */
public class HttpServerServiceTest {

    @Mock
    private GuavaServiceRepository repository;

    @Before
    public void before() {
        initMocks(this);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testAddedToRepo() {
        HttpServerService service = new HttpServerService(repository,
                                                          mock(Provider.class),
                                                          mock(HttpConfig.class));
        verify(repository).add(service);
    }
}
