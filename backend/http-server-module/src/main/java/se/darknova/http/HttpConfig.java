package se.darknova.http;

import com.netflix.archaius.api.annotations.Configuration;
import lombok.Data;

/**
 * @author simon
 */
@Configuration(prefix = "http.api")
@Data
public class HttpConfig {
    private int port;
}
