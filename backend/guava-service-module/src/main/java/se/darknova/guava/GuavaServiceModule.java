package se.darknova.guava;

import com.google.common.util.concurrent.ServiceManager;
import com.google.inject.AbstractModule;
import lombok.EqualsAndHashCode;
import javax.inject.Singleton;

/**
 * @author seamonr@gmail.com
 */
@EqualsAndHashCode(callSuper=false, of={})
public class GuavaServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(GuavaRun.class);
        bind(ServiceManager.class).toProvider(ServiceManagerProvider.class).in(Singleton.class);
    }

}
