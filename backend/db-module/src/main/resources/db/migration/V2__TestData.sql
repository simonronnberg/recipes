insert into tag (name) values ('Middag');
insert into ingredient (name, description) values ('Oregano', 'Torkad Oregano');
insert into unit (name, type) values ('tsk', 'Volume');

insert into recipe (name, description) values ('Testrecept', 'Testar stuff');
insert into recipe_tag values (1, 1);
insert into recipe_item (recipe_id, name) values (1, 'Sås');
insert into recipe_ingredient (recipe_id, item_id, ingredient_id, unit_id, amount) values (1, 1, 1, 1, 1.0);
insert into recipe_instruction (recipe_id, sort_order, text) values (1, 1, 'Blanda oregano med vatten.');