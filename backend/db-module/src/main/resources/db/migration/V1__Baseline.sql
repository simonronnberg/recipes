create table recipe (
    id bigint not null generated always as identity (start with 1, increment by 1),
    name varchar(100) not null,
    description clob
);

create table ingredient (
    id bigint not null generated always as identity (start with 1, increment by 1),
    name varchar(100) not null,
    description clob
);

create table unit (
    id bigint not null generated always as identity (start with 1, increment by 1),
    name varchar(100) not null,
    type varchar(16) not null
);

create table unit_conversion (
    unit_left_id bigint not null,
    unit_right_id bigint not null,
    conversion_rate float not null
);

create table tag (
    id bigint not null generated always as identity (start with 1, increment by 1),
    name varchar(100) not null
);

create table recipe_tag (
    recipe_id bigint not null,
    tag_id bigint not null
);

create table recipe_item (
    id bigint not null generated always as identity (start with 1, increment by 1),
    recipe_id bigint not null,
    name varchar(100) not null
);

create table recipe_ingredient (
    id bigint not null generated always as identity (start with 1, increment by 1),
    recipe_id bigint not null,
    item_id bigint,
    ingredient_id bigint not null,
    unit_id bigint not null,
    amount float not null
);

create table recipe_instruction (
    id bigint not null generated always as identity (start with 1, increment by 1),
    recipe_id bigint not null,
    sort_order int not null,
    text clob
);