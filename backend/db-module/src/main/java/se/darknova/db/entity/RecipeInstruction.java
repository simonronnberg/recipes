package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class RecipeInstruction {
    private long id;
    private int order;
    private String text;
}
