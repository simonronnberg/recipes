package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class RecipeItem {
    private long id;
    private String name;
}
