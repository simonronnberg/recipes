package se.darknova.db;

import com.google.inject.AbstractModule;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.mybatis.guice.MyBatisModule;
import se.darknova.db.mapper.IngredientMapper;
import se.darknova.db.mapper.RecipeIngredientMapper;
import se.darknova.db.mapper.RecipeInstructionMapper;
import se.darknova.db.mapper.RecipeItemMapper;
import se.darknova.db.mapper.RecipeMapper;
import se.darknova.db.mapper.TagMapper;
import se.darknova.db.mapper.UnitMapper;

import javax.sql.DataSource;

/**
 * @author simon
 */
public class DBModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new MyBatisModule() {

            @Override
            protected void initialize() {
                environmentId("dev");
                bindDataSourceProviderType(HikariDataSourceProvider.class);
                bindTransactionFactoryType(JdbcTransactionFactory.class);
                addMapperClass(RecipeMapper.class);
                addMapperClass(IngredientMapper.class);
                addMapperClass(RecipeIngredientMapper.class);
                addMapperClass(RecipeItemMapper.class);
                addMapperClass(RecipeInstructionMapper.class);
                addMapperClass(TagMapper.class);
                addMapperClass(UnitMapper.class);
            }
        });
        bind(DataSource.class).toProvider(HikariDataSourceProvider.class).asEagerSingleton();
    }

}
