package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.Ingredient;
import se.darknova.db.entity.RecipeIngredient;
import se.darknova.db.entity.RecipeItem;
import se.darknova.db.entity.Unit;

import java.util.List;

/**
 * @author simon
 */
public interface RecipeIngredientMapper {

    @Select("SELECT * FROM recipe_ingredient WHERE recipe_id=#{recipeId}")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "amount", column = "amount"),
        @Result(property = "ingredient", column = "ingredient_id", javaType = Ingredient.class,
                one = @One(select = "se.darknova.db.mapper.IngredientMapper.get")),
        @Result(property = "unit", column = "unit_id", javaType = Unit.class,
                one = @One(select = "se.darknova.db.mapper.UnitMapper.get")),
        @Result(property = "item", column = "item_id", javaType = RecipeItem.class,
                one = @One(select = "se.darknova.db.mapper.RecipeItemMapper.get")),
    })
    List<RecipeIngredient> listIngredientsForRecipe(long recipeId);

    @Insert("INSERT INTO recipe_ingredient (recipe_id, ingredient_id, item_id, unit_id, amount) "
            + "VALUES (#{recipeId}, #{ingredient.id}, #{item.id}, #{unit.id}, #{amount})")
    void add(@Param("recipeId") long recipeId, RecipeIngredient ingredient);

    @Update("UPDATE recipe_ingredient SET item_id=#{item.id}, unit_id=#{unit.id}, amount=#{amount} WHERE id=#{id}")
    void update(RecipeIngredient ingredient);

    @Delete("DELETE FROM recipe_ingredient WHERE id=#{id}")
    void remove(long id);
}
