package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.Tag;

import java.util.List;

/**
 * @author simon
 */
public interface TagMapper {

    @Select("SELECT * FROM tag WHERE id = #{id}")
    Tag get(long id);

    @Select("SELECT * FROM tag")
    List<Tag> list();

    @Select("SELECT t.* FROM tag t INNER JOIN recipe_tag r ON t.id = r.tag_id WHERE r.recipe_id = #{recipeId}")
    List<Tag> listTagsForRecipe(long recipeId);

    @Insert("INSERT INTO tag(name) VALUES(#{name})")
    @Options(useGeneratedKeys = true)
    int insert(Tag tag);

    @Update("UPDATE tag SET name=#{name} WHERE id=#{id}")
    void update(Tag tag);

    @Delete("DELETE FROM tag WHERE id=#{id}")
    void delete(long id);
}
