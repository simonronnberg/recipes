package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class RecipeIngredient {
    private long id;
    private RecipeItem item;
    private Ingredient ingredient;
    private Unit unit;
    private float amount;
}
