package se.darknova.db;

import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.flywaydb.core.Flyway;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

/**
 * @author simon
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
class HikariDataSourceProvider implements Provider<DataSource> {

    private final DBConfig dbConfig;

    @Override
    public DataSource get() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(dbConfig.getDriver());
        dataSource.setJdbcUrl(dbConfig.getUrl());
        if (dbConfig.isDoMigrations()) {
            Flyway flyway = new Flyway();
            flyway.setDataSource(dataSource);
            flyway.migrate();
        }
        return dataSource;
    }
}
