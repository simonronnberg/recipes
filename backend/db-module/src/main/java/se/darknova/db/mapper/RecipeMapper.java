package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.Recipe;

import java.util.List;

/**
 * @author simon
 */
public interface RecipeMapper {

    @Select("SELECT * FROM recipe WHERE id=#{id}")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "description", column = "description"),
        @Result(property = "tags", javaType = List.class, column = "id",
                many = @Many(select = "se.darknova.db.mapper.TagMapper.listTagsForRecipe")),
        @Result(property = "items", javaType = List.class, column = "id",
                many = @Many(select = "se.darknova.db.mapper.RecipeItemMapper.listItemsForRecipe")),
        @Result(property = "instructions", javaType = List.class, column = "id",
                many = @Many(select = "se.darknova.db.mapper.RecipeInstructionMapper.listInstructionsForRecipe")),
         @Result(property = "ingredients", javaType = List.class, column = "id",
                 many = @Many(select = "se.darknova.db.mapper.RecipeIngredientMapper.listIngredientsForRecipe"))
    })
    Recipe get(long id);

    @Select("SELECT * FROM recipe")
    @Results({
                 @Result(property = "id", column = "id"),
                 @Result(property = "name", column = "name"),
                 @Result(property = "description", column = "description"),
                 @Result(property = "tags", javaType = List.class, column = "id",
                         many = @Many(select = "se.darknova.db.mapper.TagMapper.listTagsForRecipe")),
                 @Result(property = "items", javaType = List.class, column = "id",
                         many = @Many(select = "se.darknova.db.mapper.RecipeItemMapper.listItemsForRecipe")),
                 @Result(property = "instructions", javaType = List.class, column = "id",
                         many = @Many(select = "se.darknova.db.mapper.RecipeInstructionMapper.listInstructionsForRecipe")),
                 @Result(property = "ingredients", javaType = List.class, column = "id",
                         many = @Many(select = "se.darknova.db.mapper.RecipeIngredientMapper.listIngredientsForRecipe"))
             })
    List<Recipe> list();

    @Insert("INSERT INTO recipe(name, description) VALUES(#{name}, #{description})")
    @Options(useGeneratedKeys = true)
    int insert(Recipe recipe);

    @Update("UPDATE recipe SET name=#{name}, description=#{description} WHERE id=#{id}")
    void update(Recipe recipe);

    @Delete("DELETE FROM recipe WHERE id=#{id}")
    void delete(long id);

    @Insert("INSERT INTO recipe_tag VALUES(#{recipeId}, #{tagId})")
    void insertTag(long recipeId, long tagId);

    @Insert("DELETE FROM recipe_tag WHERE recipe_id=#{recipeId} AND tag_id=#{tagId}")
    void deleteTag(long recipeId, long tagId);
}
