package se.darknova.db;

import com.netflix.archaius.api.annotations.Configuration;
import lombok.Data;

/**
 * @author simon
 */
@Data
@Configuration(prefix = "db")
class DBConfig {
    private String url;
    private String driver;
    private boolean doMigrations = false;
}
