package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class Tag {
    private long id;
    private String name;
}
