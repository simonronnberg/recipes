package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.Unit;

import java.util.List;

/**
 * @author simon
 */
public interface UnitMapper {

    @Select("SELECT * FROM unit WHERE id=#{id}")
    Unit get(long id);

    @Select("SELECT * FROM unit")
    List<Unit> list();

    @Select("SELECT * FROM unit WHERE type=#{type}")
    List<Unit> listForType(Unit.Type type);

    @Select("SELECT conversion_rate FROM unit_conversion "
            + "WHERE unit_left_id=#{left.id} AND unit_right_id=#{right.id}")
    float getRate(Unit left, Unit right);

    @Insert("INSERT INTO unit_conversion VALUES (#{left.id}, #{right.id}, #{rate}")
    void insertRate(Unit left, Unit right, float rate);

    @Delete("DELECT FROM unit_conversion WHERE unit_left_id=#{left.id} AND unit_right_id=#{right.id}")
    void removeRate(Unit left, Unit right);

    @Insert("INSERT INTO unit(name, type) VALUES (#{name}, #{type})")
    @Options(useGeneratedKeys = true)
    void insert(Unit unit);

    @Update("UPDATE unit SET name=#{name}, type=#{type} WHERE id = #{id}")
    void update(Unit unit);

    @Delete("DELETE FROM unit WHERE id = #{id}")
    void delete(long id);
}
