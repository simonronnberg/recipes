package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.RecipeItem;

import java.util.List;

/**
 * @author simon
 */
public interface RecipeItemMapper {

    @Select("SELECT * FROM recipe_item WHERE recipe_id=#{recipeId}")
    List<RecipeItem> listItemsForRecipe(long recipeId);

    @Select("SELECT * FROM recipe_item WHERE id=#{itemId}")
    RecipeItem get(long itemId);

    @Insert("INSERT INTO recipe_item (recipe_id, name) VALUES(#{recipeId}, #{name}")
    @Options(useGeneratedKeys = true)
    void add(@Param("recipeId") long recipeId, RecipeItem item);

    @Update("UPDATE recipe_item SET name=#{name} WHERE id=#{id}")
    void update(RecipeItem item);

    @Delete("DELETE FROM recipe_item WHERE id=#{id}")
    void remove(long id);
}
