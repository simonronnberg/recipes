package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.RecipeInstruction;

import java.util.List;

/**
 * @author simon
 */
public interface RecipeInstructionMapper {

    @Select("SELECT * FROM recipe_instruction WHERE recipe_id=#{recipeId}")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "order", column = "sort_order"),
        @Result(property = "text", column = "text"),
    })
    List<RecipeInstruction> listInstructionsForRecipe(long recipeId);

    @Insert("INSERT INTO recipe_instruction (recipe_id, sort_order, text) VALUES (#{recipeId}, #{order}, #{text})")
    @Options(useGeneratedKeys = true)
    void add(@Param("recipeId") long recipeId, RecipeInstruction instruction);

    @Update("UPDATE recipe_instruction SET sort_order=#{order}, text=#{text} WHERE id=#{id}")
    void update(RecipeInstruction instruction);

    @Delete("DELETE FROM recipe_instruction WHERE id=#{id}")
    void remove(long id);
}
