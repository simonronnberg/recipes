package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class UnitConversion {
    private Unit leftUnit;
    private Unit rightUnit;
    private float rate;
}
