package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class Ingredient {
    private long id;
    private String name;
    private String description;
}
