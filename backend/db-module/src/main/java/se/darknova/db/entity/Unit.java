package se.darknova.db.entity;

import lombok.Data;

/**
 * @author simon
 */
@Data
public class Unit {

    public enum Type {
        Weight,
        Volume,
        Number
    }

    private long id;
    private String name;
    private Type type;
}
