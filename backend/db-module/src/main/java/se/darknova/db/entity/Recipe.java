package se.darknova.db.entity;

import lombok.Data;

import java.util.List;

/**
 * @author simon
 */
@Data
public class Recipe {
    private long id;
    private String name;
    private String description;

    private String bannerImageUrl;
    private String posterImageUrl;

    private List<Tag> tags;
    private List<RecipeItem> items;
    private List<RecipeInstruction> instructions;
    private List<RecipeIngredient> ingredients;
}