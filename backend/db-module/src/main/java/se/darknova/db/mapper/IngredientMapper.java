package se.darknova.db.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import se.darknova.db.entity.Ingredient;

import java.util.List;

/**
 * @author simon
 */
public interface IngredientMapper {

    @Select("SELECT * FROM ingredient WHERE id=#{id}")
    Ingredient get(long id);

    @Select("SELECT * FROM ingredient")
    List<Ingredient> list();

    @Insert("INSERT INTO ingredient (name, description) VALUES (#{name}, #{description}")
    @Options(useGeneratedKeys = true)
    void insert(Ingredient ingredient);

    @Update("UPDATE ingredient SET name=#{name}, description=#{description} WHERE id=#{id}")
    void update(Ingredient ingredient);

    void delete(long id);

}